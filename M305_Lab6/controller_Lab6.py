'''
@file controller_Lab6.py
@brief  A proportional controller is used to match the angular velocity of the motor to the reference angular velocity value.
@details This file contains the closed loop driver as well as the closed loop task that runs one iteration of the closed loop driver. 
         The closed loop driver contains methods for setting the proportional constant and calculating a duty cycle from the error in the angular velocity measurement.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date March 7, 2021
'''

import utime
import micropython
import shares_Lab6

## Allcates 200 bytes of memory for errors due to interrupt
micropython.alloc_emergency_exception_buf(200)

class ClosedLoop:
    '''
    @brief      The input angular velocity of the motor is controlled with a proportional controller.   
    @details    The two angular velocity is determined from encoder values and the resulting PWM level, L, is the input to the motor driver.
                This is what controls the duty cycle of the motor.
    '''
    
    def __init__(self, Kp):
        '''
        @brief     Creates a closed loop driver object. The constructor takes in the proportional constant set by the user.
        @param     Kp   This is the value chosen for the proportional control of the motor's angular velocity.
        '''
        ## Constant used for proportional control.
        self.Kp = Kp
        ## Variable for the pulse width modulation level of the motor.
        self.PWM_L = 0
        
    def run(self, omega_ref, omega_meas):
        '''
        @brief   Calculates the required pulse width modulation level to achieve the reference angular velocity. 
        @param   omega_ref  The target or reference angular velocity of the motor.
        @param   omega_meas The measured angular velocity of the motor.
        @return PWM_L The duty cycle for controlling the motor with pulse width modulation.
        '''     
        self.PWM_L = self.Kp * (omega_ref - omega_meas)
        if self.PWM_L < 0:
            shares_Lab6.L = 0
        elif self.PWM_L > 100:
            shares_Lab6.L = 100
        else:
            shares_Lab6.L = self.PWM_L
        
    def get_Kp(self):
        '''
        @brief    Retrieve the current proportional constant value. 
        @return Kp  Returns the constant for proportional control.
        '''
        return self.Kp
    
    def set_Kp(self, Kp_new):
        '''
        @brief  Set the proportional constant value. Used to update Kp.
        '''
        self.Kp = Kp_new
        

class ClosedLoopTask:
    '''
    @brief          This class implements a finite state machine to control the ClosedLoopTask.
    @details        This class implements a finite state machine to control the ClosedLoopTask which has a total of 3 states.
    '''    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    
    ## Constant defining State 1 - Wait for command.
    S1_WAIT = 1
    
    ## Constant defining State 2 - Implement control.
    S2_CTL = 2
    
    
    def __init__(self,ClosedLoop,Encoder,Motor,interval):
        '''
        @brief              Creates a task to run the closed-loop controller 
        @param ClosedLoop   This is the parameter for passing in the closed-loop driver object.
        @param Encoder      This is the parameter for passing in the encoder driver object.
        @param Motor        This is the parameter for passing in the motor driver object.
        @param interval     The interval between each iteration of the task run() method in milliseconds.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in milliseconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval*1e6
        
        ## Defines the closed-loop driver object.
        self.ClosedLoop = ClosedLoop
        
        ## Defines the encoder driver object.
        self.Encoder = Encoder
        
        ## Defines the motor driver object.
        self.Motor = Motor
        
        ## The timestamp for the first iteration. Time in microseconds.
        self.start_time = utime.ticks_ms()
        
        ## Defines the current time for the iteration and is overwritten at the beginning of each iteration.
        self.currTime = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next.
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
        ## Initialize the duty cycle of the motor to zero.
        self.PWM_L = 0
        
        ## Initialize the number of runs the finite state machine runs for.
        self.run = 0
    
    def run(self):
        '''
        @brief    Runs one iteration of the closed loop task
        '''    
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.currTime = utime.ticks_us()
        
        # Specifying the next time the task will run
        if utime.ticks_diff(self.currTime, self.next_time) >= 0:
            # If the interval has been reached
            if self.state == self.S0_INIT:
                # Run state 0 code
                # Initialize encoder position to zero.
                self.Encoder.set_position()
                self.transitionTo(self.S1_WAIT)
                
            elif self.state == self.S1_WAIT:
                # Run state 1 code - set Kp
                self.Encoder.update()
                if shares_Lab6.Kp:                
                    self.ClosedLoop.set_Kp(shares_Lab6.Kp)
                shares_Lab6.Kp = 0.1
                shares_Lab6.omega_meas = self.Encoder.get_delta() * 10**3 * 60 / (self.interval * 4000) # Calculate rpm
                if self.run > 20:
                    shares_Lab6.omega_ref = 75
                else:
                    shares_Lab6.omega_ref = 0
                self.transitionTo(self.S2_CTL)   
            
            elif self.state == self.S2_CTL:
                # Run state 2 code
                self.ClosedLoop.run(shares_Lab6.omega_ref,shares_Lab6.omega_meas)
                self.Motor.set_duty(shares_Lab6.L*3.3)     # Set PWM level for motor 1
                self.transitionTo(self.S1_WAIT)

            self.run += 1
            ## Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self,newState):
        '''
        @brief          Updates the state variable.
        @param newState The desired state for next iteration.
        '''
        self.state = newState