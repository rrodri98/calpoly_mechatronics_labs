'''
@file UI_front_Lab6.py
@brief The user interface front end communicates to the Nucleo through serial communication. It will prompt the user to enter a proportional constant 'Kp' for data collection.
@details  At this point, the Nucleo will begin waiting for a user button press. The angular velocity and timestamps will be stored in an array then opened
        as a batch of data on the front end. 
        The data will be plotted using the matplotlib module in the the front end as well. 
@author Rebecca Rodriguez
@date February 20, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           See source for myformat function: https://stackoverflow.com/questions/2389846/python-decimals-format.
'''
import numpy as np
import matplotlib.pyplot as plt
import serial 
import time

## Set up serial port. Define serial port definition, baude rate, and timeout flag.
ser = serial.Serial(port='COM4',baudrate=115200,timeout=1)  

## @brief Empty array to store time values.
time_ = []
## @brief  Empty array to store angular velocity values.
omega = []
## @brief  Empty array to store reference angular velocity values.
omegaRef = []

# Send an ASCII character
def sendChar():
    '''
    @brief    Responsible for requesting and reading characters from the user.
    @details  The value then gets sent to the Nucleo side to verify the appropriate 
              character was sent to initialize data collection.
    '''
    # Prompts user to enter a proportional constant 'Kp' to continue with data collection.
    inv = input('Please enter a Kp value for proportional control: \r\n ')
    # Character input gets converted to decimal value and returns it to Nucleo.
    ser.write(str(inv).encode('ascii'))
    
for n in range(1):
    print(sendChar())

    for count in range (100):
        ## Variable used to store the serial inputs. The input from the Nucleo side is in the form [time, angular velocity].
        val = ser.readline().decode('ascii')
        ## Remove the carriage return and new line and split data with a comma.
        values = val.strip('\r\n').split(',')
        print(values)
        
        # Append values if no empty string is returned
        time.sleep(0.5)
        if val != '': 
            # Resulting values are strings
            omegaRef.append(values[2])
            omega.append(values[1])
            time_.append(values[0])
            
            
## Convert the list of omega strings to list of floats for plotting.
list_of_omegafloats = [float(item) for item in omega]
## Convert the list of reference omega strings to list of floats for plotting.
list_of_omegaREf_floats = [float(item) for item in omegaRef]
## Convert the list of time strings to list of floats for plotting.
list_of_timefloats = [float(item) for item in time_]

# Close serial port
ser.close()
    
# Need to plot angular velocity vs time
plt.plot(list_of_timefloats, list_of_omegafloats, label="Measured")
plt.plot(list_of_timefloats, list_of_omegaREf_floats, label="Reference")
plt.xlabel("Time [microsec]", fontsize = 18)
#plt.xticks(np.arange(0, 500, step=50))  # Set label locations.
plt.ylabel("Angular Velocity [rpm]", fontsize = 18)
plt.savefig("KP1.png")
plt.show()