'''
@file shares_Lab6.py
@brief A container for all the inter-task variables.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The proportional constant value set by the user.
Kp           = None

## The target angular velocity for testing.
omega_ref    = None

## The actual angular velocity measured from the encoders.
omega_meas   = None

## The duty level required for controlling the motor.
L            = None