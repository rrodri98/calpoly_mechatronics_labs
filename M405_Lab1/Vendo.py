'''
@file Vendo.py
@brief This assignment simulates a purchase from the Vendotron - a vending machine that sells off-market beverages. The structure is based off a finite state machine to obtain user's input and return their change in 
       the form of a tuple of the number of ($1, quarters, dimes, nickels, pennies).
       The finite state machine executes in a while loop.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           See source for myformat function: https://stackoverflow.com/questions/2389846/python-decimals-format.
@date April 22, 2021
'''

import keyboard
from time import sleep
import time

last_key = ''
## List of usable key inputs
button_keys = ['c','p','s','d','e','q','0','1','2','3','4','5','6','7']

def kb_cb(key):
    """ 
    @brief  Callback function which is called when a key has been pressed.
    @param key The user's keypress is stored.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
for button in button_keys:
    keyboard.on_release_key(button, callback=kb_cb)

def printWelcome():
    '''
    @brief      This function greets the user who comes across the Vendotron.
    @details    No parameters are returned for this function. The print statement of this function may be changed to display a different welcome display message.
    '''
    print('Enter c-Cuke, p-Popsi, s-Spryte, d-Dr.Pupper, 0-penny, 1-nickel, 2-dime, 3-quarter, 4-$1, 5-$5, 6-$10, 7-$20')
    pass

def returnChange(balance):
    '''
    @brief      This function returns the user change from the virtual Vendotron.
    @details    The balance is returned to the user. This function only executes if there is a valid balance to return. If 
                money is owed or if the balance is zero this function does not execute and no money is returned to the user.
    @param   balance   The input to the function is the user's balance. 
    @return     The user's change is printed in the form of a tuple.
    '''
    ## The Vendotron may return user change in $1 dollar bills, quarters, dimes, nickels, and pennies. This may be edited so that the 'Vendotron' cash register can return change in coins only.
    cashRegister = {'Ones':100, 'Quarters': 25, 'Dimes':10, 'Nickels':5, 'Pennies': 1}
    ch = [0, 0, 0, 0, 0]
    i = 0
    run = 0
    for key in cashRegister:
        if (balance - cashRegister[key] < 0):
            VAL = 0
            ch[i] = VAL
            i +=1
            continue
        else:
            while (balance - cashRegister[key] > 0):
                balance -= cashRegister[key]
                run += 1
                while (balance - cashRegister[key] > 0):
                    balance -= cashRegister[key]
                    run += 1
                ch[i] = run
                break
            i +=1
            run = 0
    ch[4] += int(balance)     
    print(tuple(ch)) 
    return 
    
def VendotronTask():
    '''
    @brief      This portion of code runs one iteration of the finite state machine while the statement is true, which is always.
    @details    The finite state machine runs through the cycles of accepting user payment, validating and processing the payment, as well as returning the correct drink with any leftover change if the user chooses to do so.
                At any time time the user may select their preferred beverage or press 'e' to get their full balance back.
    '''
    ## Initialize the first state in the state diagram
    state = 0
    ## This is the user balance that gets returned after each transaction.
    balance = 0
    ## The cost of the drink is initialized as $0.00 (0 cents)
    cost = 0
    ## This dictionary sequence is used to relate the user input to its corresponding value in cents. The values correspond to $20, $10, $5, $1, $0.25, $0.10, $0.05, $0.01 respectively.
    UserInput = {7: 2000, 6 :1000, 5: 500, 4:100, 3: 25, 2:10, 1:5, 0: 1}
    ## This dictionary sequence correlates the drink to corresponding cost of the drink.
    drinkRegister = {'c': 100, 'p':120, 's': 85, 'd': 110}
    
    payment = None
    count = 0
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C        
        if state == 0:
            '''
            Run State 0 - Initializes Vendotron.
            '''
            print('Powering Vendotron On...')
            time.sleep(2)
            printWelcome()
            print('Make a selection:')
            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            '''
            Runs State 1 Code - Will prompt user to 'Try a Cuke!' if response time is slower than 10 seconds.
            '''            
            global last_key

            if last_key == '' :
                time.sleep(0.5)
                count +=1
                if count == 20:
                    count = 0
                    print('Try a Cuke! ')
                state = 1
            elif last_key != '':
                state = 2
        
        elif state == 2:
            '''
            Runs State 2 Code - Welcomes user to the vending machine and prompts user for a payment.
            '''
            count = 0
            if last_key == 'c' or last_key == 'p' or last_key == 's' or last_key == 'd':
                state = 3       # on the next iteration, the FSM will run state 3
            elif last_key == '0' or last_key == '1' or last_key == '2' or last_key == '3' or last_key == '4' or last_key == '5' or last_key == '6' or last_key == '7':
                payment = int(last_key)
                last_key = ''
                state = 4       # on the next iteration, the FSM will run state 4
            elif last_key == 'e':
                if balance < 0:
                    print('Total cost: $0, Balance: $0')
                    print('No balance to return. ')
                else:
                    print('Eject! Return: $' + str(balance/100))
                    returnChange(balance)
                balance = 0
                cost = 0
                printWelcome()
                last_key = ''
                state = 1       #on the next iteration, the FSM will run state 1
            elif last_key == 'q':
                print("'q' detected. Quitting...")
                keyboard.unhook_all ()
            state = 3   # on the next iteration, the FSM will run state 3
            
        elif state == 3:
            '''
            Runs State 3 Code - Accept user input to determine cost owed. Display the cost of each drink.
            '''
            if last_key in drinkRegister:
                cost += drinkRegister[last_key]
                balance -= drinkRegister[last_key]
                if balance < cost:
                    print('Total cost: $' + str(cost/100) + ', Balance: $' + str(balance/100))
                    print('Insufficient funds. ')
                elif balance > cost: 
                    print('Total cost: $0, Balance: $' + str(balance/100))
                    print('Enjoy your drink! ')
                    if (balance/100) >= 0.85:
                        print("You have suffient funds to select another drink otherwise press 'e' to get your change! ")
                else:
                    print('Enjoy your drink!')
                    printWelcome()
                last_key = ''
            state = 4   # on the next iteration, the FSM will run state 4
            
        elif state == 4:
            '''
            Runs State 4 Code - Accept user payment. Process user payment & display balance.
            '''
            if payment in UserInput:
                balance += UserInput[payment]
                cost = 0
                last_key = ''
                payment = None
                if balance < cost:
                    print('Total cost: $' + str(cost/100) + ', Balance: $' + str(balance/100))
                    print('Insufficient funds. ')
                elif balance > cost and cost > 0: 
                    print('Total cost: $0, Balance: $' + str(balance/100))
                    print('Enjoy your drink! ')
                    if (balance/100) >= 0.85:
                        print("You have suffient funds to select another drink otherwise press 'e' to get your change! ")
                elif balance > cost and cost == 0: 
                    print('Total cost: $0, Balance: $' + str(balance/100))
                    if (balance/100) >= 0.85:
                        print("You have suffient funds to select a drink otherwise press 'e' to get your change! ")
                else:
                    print('Total cost: $0, Balance: $0') # balance = cost exactly so don't need to give change
                    print('Enjoy your drink!')
                    time.sleep(2)
                    printWelcome()
                    
            state = 1   # on the next iteration, the FSM will run state 1
        
        yield(state)


vendo = VendotronTask()

try:
    while True:
        next(vendo)
        sleep(0.01)
        
except KeyboardInterrupt:
    print('Ctrl-c detected. Goodbye')
        
except StopIteration:
    print('Task has stopped yielding its state. Check your code.')

if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')