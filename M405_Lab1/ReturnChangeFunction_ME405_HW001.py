'''
@file ReturnChangeFunction_ME405_HW001.py
@brief This function computes the correct change for a given purchase. The function returns change with the fewest number of bills and coins.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           See source for myformat function: https://stackoverflow.com/questions/2389846/python-decimals-format.
'''
def myformat(val):
    '''
    @brief      This function correctly formats the change output and strips any extraneous zero decimal place holders.
    @details    The output has only two decimal places.
    @param  val This inut parameter any value that needs to be formatted to the hundredths place.
               '''         
    return ('%d' % val).rstrip('0').rstrip('.')


def getChange(price, payment):
    '''
    @brief      This function computes the change for a virtual purchase.
    @details    The change is given in the fewest denominations possible.
    @param  price This is the cost of the purchase given in cents.
    @param  payment  This is the amount that the user attempts to pay for their purchase given as a tuple of the form (pennies, nickels, dimes, quarters, $1, $5, $10, $20).
    ''' 
    while True:
        try:
            if price < 0:
                # Error if price is negative.
                print('Error. Cost of purchase cannot be negative. ')
                break
            
            if price >= 0:
                # Convert price in cents to equivalent dollar amount if price amount is valid.
                cost_d = price/100
                
                # Accept user's payment as a tuple
                Payment = tuple(payment)
                UserPayment_Cents = Payment[0] * 0.01 + Payment[1] * 0.05 + Payment[2] * 0.10 + Payment[3] * 0.25
                UserPayment_Dollars = Payment[4] * 1.00 + Payment[5] * 5.00 + Payment[6] * 10.00 + Payment[7] * 20.00
                Payment_in_cents = (UserPayment_Cents * 100) + (UserPayment_Dollars * 100)
                print('You would like to make a purchase of $' + str(cost_d) + ' with $' + str(round((Payment_in_cents/100),2)))
                break

        except ValueError:
            print('Please enter a valid cost of purchase. ')
            break
            
        finally:
            pass
          
 
    if (Payment_in_cents < price):
        print('Insufficient funds.')
    else:
        # Compute change if there are sufficient funds
        change_cents_total = Payment_in_cents - price
        print('Your change is $' + str(change_cents_total/100))
        
        register = {'Twenties': 2000, 'Tens' :1000, 'Fives': 500, 'Ones':100, 'Quarters': 25, 'Dimes':10, 'Nickels':5, 'Pennies': 1}
        
        # print('Your change in cents is: ' + str(change_cents_total))
        ch = [0, 0, 0, 0, 0, 0, 0, 0]
        
        count = 0
        run = 0
        
        for key in register:
            
            if (change_cents_total - register[key] < 0):
                VAL = 0
                ch[count] = VAL
                # print('Number of ' + key + ' is ' + str(ch[count]))
                count +=1
                continue

            else:
                while (change_cents_total - register[key] > 0):
                    change_cents_total -= register[key]
                    run += 1
                    
                    while (change_cents_total - register[key] > 0):
                        change_cents_total -= register[key]
                        run += 1
                        
                    # print('Number of ' + key + ' is __ and the leftover amount is ' + str(change_cents_total))
                    ch[count] = run
                    # print(ch)
                    break
                # ch[count] = run
                count +=1
                run = 0
                # print('Number of ' + key + ' is ' + str(ch[count]))
        # num_pennies = change_cents_total      
        # print(change_cents_total)
        ch[7] += int(change_cents_total)
        
        print(tuple(ch))
    
    return 

if __name__ == "__main__":
    # getChange(157, (3, 0, 0, 2, 1, 0, 0, 1))
    # getChange(157, (3, 0, 0, 0, 0, 0, 0, 0))
    # getChange(157, (0, 0, 0, 0, 0, 1, 0, 0))
    getChange(157, (0, 0, 0, 0, 0, 0, 0, 1))



  