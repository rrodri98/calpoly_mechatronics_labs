'''
@file UI_EncoderLab.py
@brief The user interface front end communicates to the Nucleo through serial communication. It will prompt the user to enter 'G' for data collection.
        Encoder values and timestamps will be recorded until the user presses 'S'. The batch of data sent from the Nucleo will be sent to the PC side and
        stored in a comma-separated variable file.
@author Rebecca Rodriguez
@date February 20, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           See source for myformat function: https://stackoverflow.com/questions/2389846/python-decimals-format.
'''

import matplotlib.pyplot as plt
import serial 
import csv
import matplotlib.ticker as ticker


# Set up serial port. Define serial port definition, baude rate, and timeout flag
ser = serial.Serial(port='COM8',baudrate=115200,timeout=1)  

## @brief Initialize time array.
time_ = []
## @brief  Initialize the position array.
ENC_pos = []

# Send an ASCII character
def sendChar():
    '''
    @brief    Responsible for requesting and reading characters from the user.
    @details  The value then gets sent to the Nucleo side to verify the appropriate 
              character was sent to initialize data collection.
    '''
    # Prompts user to enter 'G' to continue with data collection.
    inv = input("Enter 'G' to commence encoder data collection (Press 'S' to stop.): \r\n ")
    # Character input gets converted to decimal value and returns it to Nucleo.
    ser.write(str(inv).encode('ascii'))
    
# Keep file open while writing data to it
with open('encoder_data.csv','w') as csv_file:
    csv_writer = csv.writer(csv_file)
    for n in range(1):
        print(sendChar())
        for count in range (50):
            # Obtain the two serial inputs from the Nucleo side [Encoder position, Time]
            val = ser.readline().decode('ascii')
            # Remove the carriage return and new line + Split data with a comma
            values = val.strip('\r\n').split(',')
            # Write to CSV file
            csv_writer.writerow(values)
            print(values)
            
            # Append values if no empty string is returned
            if val != '': 
                # Resulting values are strings
                ENC_pos.append(values[1])
                time_.append(values[0])
            
            
# Convert the list of strings to list of floats
list_of_encfloats = [float(item) for item in ENC_pos]
list_of_timefloats = [float(item) for item in time_]

# Close serial port
ser.close()
    
# Need to plot encoder position vs time
plt.plot(list_of_encfloats,list_of_timefloats)
plt.xlabel("Time [Sec]", fontsize = 18)
ax = plt.axes()
ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))
ax.yaxis.set_major_locator(ticker.MultipleLocator(100))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(100))
plt.ylabel("Encoder Position [Deg]", fontsize = 18)
plt.savefig("EncoderTrial_1.png")
plt.show()
