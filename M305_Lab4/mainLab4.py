'''
@file mainLab4.py
@brief    This will run on the Nucleo to facilitate the software handshake. The file will run on the Nucleo startup because file named main.py therefore, command execfile is not needed.
          Encoder values and timestamps will be recorded once the user presses 'G' from the console in Spyder.The user may terminate data collection at any time by pressing 'S'.
@author Rebecca Rodriguez
@date February 20, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
import pyb
from pyb import UART
from EncoderLabMain import EncoderDriver 
import array as array
import micropython
import utime

## Allcates 200 bytes of memory for errors due to interrupt
micropython.alloc_emergency_exception_buf(200)

## @brief Will need a buffer to store the encoder values
response = array.array('H', (0 for index in range (50)))
## @brief Knowing the frequency at which the ADC samples gives the corresponding time stamp
t_step = 0.2
## @brief Will need a buffer to store the time
time = array.array('H', (0 for index in range (50)))

## Create serial object for communication
myuart = UART(2)
A6 = pyb.Pin.cpu.A6
A7 = pyb.Pin.cpu.A7

enc = EncoderDriver(A6, A7, 3)

interval = int(0.2*1e6)

'''
    @brief   This script serves as the backend of the Nucleo communication. 
    @details Data collection commences if the user sent a 'G' on the front end.
             Furthermore, data is terminated once the user sends 'S'.
'''
while True:
    
    if myuart.any() != 0:
        # Read user input
        val = myuart.readchar()
        
        # Read from pin if G was sent
        if val == 71:
            
            start_time = utime.ticks_us()
            
            
            # next_time = utime.ticks_add(start_time, interval)
            # #utime.delay(2)

            # #curr_time = utime.ticks_us()
            # if utime.ticks_diff(start_time, next_time) >= 0:
            #     #print('Done')
            #     enc.update()
            #     myuart.write('{:} \r\n'.format(enc.get_position()))
            #     #myuart.write('{:},{:}\r\n'.format (utime.ticks_us(), enc.get_position())) 
            #     #myuart.write('{:}\r\n'.format (utime.ticks_us())) 
            # # Store data in an array
            #     # Stop data collection if S is pressed
            #     if val == 83:
            #         break            
            # pass
            
            #start_time = utime.ticks_us()
            
            #myuart.write('{:},{:}', utime.ticks_us(),enc.get_position())
           
            #myuart.write('{:2.2f} \r\n',enc.get_position())
            # next_time = utime.ticks_us()
            # if utime.ticks_diff(start_time, next_time) >= 0:
            #     pass
        
            # while val == 71:
            for i in range(50):
                enc.update()
                utime.sleep_us(200000)
                myuart.write('{:2.2f},{:2.2f}\r\n'.format(enc.get_position(),(utime.ticks_us()-start_time)*1e-6))
        if val == 83:
            pass
            

        # 'G' was not pressed. Prompt user to press 'G.'
        else:
            myuart.write("Press 'G' for data collection. \r\n")
            break