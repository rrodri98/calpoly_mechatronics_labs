'''
@file BLE_Module.py
@brief  Blinks LED according to a square wave pattern with the
        frequency set by the user. The user input
        is sent from a phone to the NUCLEO using a bluetooth
        antenna and app interface made through Thunkable.
        
@author Rebecca Rodriguez
@date March 29, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           
'''


import pyb
from pyb import UART
import utime


class BLE_Driver:
    
    '''
    @brief      A finite state machine to control the BLE_Driver.
    @details    This class implements a finite state machine to control the
                bluetooth module driver. It contains methods to read and send characters from the phone to the microcontroller, as well as methods to turn the LED on and off.
    
    '''
    ## Initialization state.
    S0_INIT             = 0
    
    ## Wait for command state.
    S1_WAIT_FOR_CMD     = 1
    
    ## Wait for response state.
    S2_WAIT_FOR_RESP    = 2
    
    def __init__(self, interval):
        '''
        @brief           The constructor sets the interval and zeros the variables.
        @param interval Parameter for the number of microseconds run between the task intervals.
        '''
        
        ## Initialize UART port and set baudrate to work with BT antenna.
        self.uart = UART(3, baudrate = 9600)
        
        ## Initialize LED as an ouput.
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## Create a timer object.
        self.tim = pyb.Timer(2)
        
        ## Initialize the blinking rate in [Hz].
        self.frequency = 0
        
        ##  The amount of time in microseconds between runs of the task.
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
            
        ## The timestamp for the first iteration.
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next.
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        

    def run(self):
        '''
        Runs one iteration of the task. Turns the LED on or off as specified by the user input for frequency.
        '''

        ## The timestamp for the current time in microseconds.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                self.UART_any()             
                self.transitionTo(self.state)
                
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                self.UART_read()
                ## The required period calculated from the user input in Hertz.
                self.period = round(1/self.val, 3)
                self.curr_time = utime.ticks_us()
                self.next_time = utime.ticks_add(self.start_time, self.period*1e6)
                if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    if(self.LED_ON):
                        self.LED_OFF()
                    else:
                        self.LED_ON()
                ## Writes current frequency to phone.
                self.string = 'The LED is blinking at ' + str(self.val) + ' Hz.'
                self.UART_write(self.string)
                self.start_time = utime.ticks_us()
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            else:
                # Invalid state code (error handling)
                pass
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        		
    
    ## Turns LED on
    def LED_ON(self):
        self.pinA5.high()
    
    ## Turns LED on
    def LED_OFF(self):
        self.pinA5.low()
        
        
    ## Checks to see if any characters were sent from phone
    def UART_any(self):
        if self.uart.any() != 0:
            self.state = self.S2_WAIT_FOR_RESP
            return self.uart.any()
        else:
            self.state = self.S1_WAIT_FOR_CMD


    ## Read from the UI and perform error checking
    def UART_read(self):
        try:
            ## Variable storing the user input. Error checking is used to only allow frequencies from 1-10 Hz.
            self.val = int(self.uart.readline())
            if self.val > 1 and self.val < 10:           
                return self.val
            else:
                self.uart.write('Please enter a valid value for LED frequency. Try 1-10 Hz! \r\n')
                return None
        except ValueError:
            return None       
    
    ## Sends value to NUCLEO to change blinking frequency. User inputs frequency in [Hz].
    def UART_write(self, string):
        self.myuart.write(str(string) + '\r\n')
        return
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState