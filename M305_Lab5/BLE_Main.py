'''
@file BLE_Main.py
@brief  Blinks LED according to square wave pattern with the
        frequency set by the user input. The user input
        is sent from a phone to the NUCLEO using a bluetooth
        antenna. This is the main file to be ran.
        
@author Rebecca Rodriguez
@date March 29, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           
'''
from BLE_Module import BLE_Driver

## Set bluetooth task to run at a set interval.
BT_task1 = BLE_Driver(0.001)

## Run task indefinitely 
while True:
    BT_task1.run()