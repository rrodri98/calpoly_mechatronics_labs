'''
@file measNucleo_temp.py
@brief This script measures the internal temperature of the Nucleo-L47RG and saves it to a file.
@author Justin Slavick and Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date February 7, 2021
'''

import pyb 
import utime

class TaskInternalTemp:
    '''
    @brief This class facilitates temperature data collection onboard the Nucleo. 
    '''

    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_INTERNAL_TEMP  = 1  
    
    def __init__(self, interval):
        '''
        @brief Creates an internal temperature task object.
        @param interval An integer number of milliseconds between desired runs of the task
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration / Time in microseconds
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Max storage array
        self.intData = 480 * [0]
        
        ## Set up 12 bit resolution, internal channels
        self.adcall = pyb.ADCAll(12, 0x70000) 
        pass
    
    def run(self):
        '''
        @brief Collects the internal temperature of the microcontroller every 60 seconds.
        @return Internal temperature data collected is returned as an array.
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_ms()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
             if(self.state == self.S0_INIT):
                ## Run State 0 Code
                print('Beginning internal temp data collection...')
                self.state = self.S1_GET_INTERNAL_TEMP
                
             if(self.state == self.S1_GET_INTERNAL_TEMP_INIT):
                self.intTemp = self.adcall.read_core_temp()
                self.intData[self.runs] = self.intTemp
            
             self.runs += 1
            
             # Specifying the next time the task will run
             self.next_time = utime.ticks_add(self.next_time, self.interval)

        return self.intData
