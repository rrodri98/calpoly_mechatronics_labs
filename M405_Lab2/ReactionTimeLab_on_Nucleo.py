'''
@file ReactionTimeLab_on_Nucleo.py
@brief For this assignment, the user gets to measure how fast they react to a blinking light when it turns on randomly. Hardware is required. For this lab, the microcontroller of choice
       is the Nucleo-L47RG. When this file is ran, it will prompt the user to press the blue button on the board in response to the light. The reaction time is returned.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
import random 
import utime
import micropython

## Allcates 200 bytes of memory for errors due to interrupt
micropython.alloc_emergency_exception_buf(200)

## @brief This parameter holds the value at which the timer is pressed.
#  @details The reaction time is measured in seconds.
timeBpressed = 0

## Calback function to run when button is pressed
def button_isr (which_pin):        # Create an interrupt service routine
    '''
    @brief            This is the function that runs when the button is pressed. It returns the time at which this event occurs.
    @details          This callback function was only able to run when it was made as simple as possible, so it's sole purpose is to return the time.
    @param which_pin  This is the pin for which the interrupt service routine is set for. In this case it is Pin 13, which corresponds to the button.
    '''
    global timeBpressed
    timeBpressed = utime.ticks_us()
    # global curr_count
    # curr_count +=1

    
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
              pyb.ExtInt.IRQ_FALLING,      # Interrupt on falling edge
              pyb.Pin.PULL_UP,             # Activate pullup resistor
              button_isr)                  # Interrupt service routine


## Calback function to run when timer overflows
def Overflow(timTimer):
    '''
    @brief            This function is designed to handle timer overflows.
    @details          This occurs when the user response takes too long and exceeds the period value. The period value was set
                      for the maximum possible amount as 0x7FFFFFFF. For the sake of simplicity, no print statements were used
                      so that it won't cause runtime errors during execution; therefore, the timer simply restarts silently.
    @param  timTimer  This is the parameter that corresponds to the timer.
    '''
    # print('Timer has restarted. Your reaction time was too slowwwww. ')
    pass

## @brief Create timer to count once for each tick
# @details Timer will call the Overflow function when the timer overflows
tim = pyb.Timer(5, prescaler=79, period=0x7FFFFFFF, callback=Overflow)

## @brief Chooses a random number from 2-3 with 3 decimal places
#  @details The number generated is the random time the LED will be set off
randomTime = round(random.uniform(2,3),3)

## @brief Sets up LED on pinA5
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
## @brief Sets up timer object for LED at 20000 Hz using timer 2
tim2 = pyb.Timer(2, freq = 20000)
## @brief Configures channel 1 timer for LED on pinA5
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
## @brief Value corresponds to brightness of LED on pinA5
brightness = 0
## @brief Array for holding average reaction time is initialized 
avgReactionTot = 100 * [0]
## @brief Value for holding the number of times the game is run 
run = 0
## @brief Value for holding the average response time 
avg = 0

'''This function prompts the user to enter 'yes' to begin playing the reaction-time game.
   @param start The round begins once the user enter 'yes'. The user may escape the game by pressing ctrl-c.
   @return duration_sec This is the user's response time in seconds. If the user misses their chance to press the button, then this value will be negative.'''
while True:
    try:
        start = input('Think fast! Press the blue button on the Nucleo once the LED flashes. Type "yes" to continue or "ctrl-C" to start over. \n')
        
        if (start == 'yes' or start == 'Yes'):
            
            randomTime = round(random.uniform(2,3),3)
            
            print('Now watch for the green light!!!')
            ## Start the timer
            startTime = utime.ticks_us()
            
            ## Delay the randomly generated amount in seconds
            utime.sleep(randomTime)
            
            ## Check the timer
            currTime = utime.ticks_us()
            
            ## Proceed game once randomTime is exceeded
            if((currTime - startTime) >= randomTime):
                
                ## Blink the LED on pin PA5 at full brightness
                brightness = 100
                t2ch1.pulse_width_percent(brightness)
                ## Zero timer
                zeroDtimer = utime.ticks_us()
                ## Turn LED off after 1 sec
                utime.sleep(1)
                brightness = 0
                t2ch1.pulse_width_percent(brightness)
                
                ## Return the difference in time between the zeroed timer and the time at which the button was pressed.
                duration = utime.ticks_diff (timeBpressed, zeroDtimer)
                duration_sec = duration*(79/80_000_000)
                if duration_sec < 0:
                    print('Missed your chance? Try again! ')
                else:
                    avgReactionTot.append(duration_sec)
                    run += 1
                    print(round(duration_sec,3))

        else:
            print('Please enter a valid input. ')
        
                            
    except KeyboardInterrupt:
        avg = sum(avgReactionTot)/run
        avg = round(avg,3)
        print(" Thanks, for playing! Your average response time was " + str(avg) + " seconds. Let's play again! " )
        utime.sleep(4)
       
        
