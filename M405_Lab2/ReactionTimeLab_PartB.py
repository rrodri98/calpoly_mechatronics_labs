'''
@file ReactionTimeLab_Pt_B.py
@brief For this assignment, the user gets to measure how fast they react to a blinking light when it turns on randomly. Hardware is required. For this lab, the microcontroller of choice
       is the Nucleo-L47RG. When this file is ran, it will prompt the user to press the blue button on the board in response to the light. The reaction time is calculated using hardware timers. 
       Specifically the output capture is used to produce a blinking LED and the input capture measures the falling signal when the button is pressed.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb 
import utime
import micropython

## Allcates 200 bytes of memory for errors due to interrupt
micropython.alloc_emergency_exception_buf(200)

## @brief This parameter holds the value at which the timer is pressed.
timeBpressed = 0
## @brief This parameter holds the last count used in the output compare timer for the LED.
last_compare = 0
## @brief This parameter holds the user's response time.
deltaT = 0

# Used to calculate avg reaction time
_avgReactionTot = 0
## @brief Value for holding the number of times the game is run. 
run = 0
## @brief Value for holding the average response time. 
avg = 0
## @brief Value for the button press boolean.
BP = False

def OC_callback(tim_source): #this gets passed in when ISR runs
    '''
    @brief      The call back function for the output compare match timer is set to turn the LED on/off at a specified rate.
    @details    The rate at which the LED blinks is randomized by modifying the output compare match value.
    @param  tim_source The input parameter for the callback function is the timer.
               '''
    global last_compare
    last_compare = t2ch1.compare()
    last_compare += 32653
    if last_compare > 0xffff:
        last_compare -= 0xffff
    return 

def IC_callback(timTimer):
    '''
    @brief            This function is called when a falling edge is detected on pin PB3 - from the user button.
    @details          The timer counter is obtained at this moment and is later in the calculation of the total user response time.
    @param  timTimer  This is the parameter that corresponds to the timer used for input capture.
    '''
    global timeBpressed
    timeBpressed = t2ch2.capture()
    global BP
    BP = True
    

## @brief PA5 is the pin for the LED.
pinA5 = pyb.Pin (pyb.Pin.board.PA5, pyb.Pin.OUT_PP) 
## @brief PB5 is the pin used to obtain the signal from the user button.
pinB3 = pyb.Pin (pyb.Pin.board.PB3, pyb.Pin.IN, pull=pyb.Pin.PULL_NONE) 
## @brief Timer 2 is used for the input and output compare match.
tim2 = pyb.Timer (2, prescaler=2450, period=65536)
## @brief The output compare match is set on channel 1.
## @details Output compare match when count is equal to the specified compare value. The callback is initally initialized as 'None'.
t2ch1 = tim2.channel (1, pyb.Timer.OC_TOGGLE, pin=pinA5, callback=None)
#t2ch1 = tim2.channel (1, pyb.Timer.OC_TOGGLE, pin=pinA5, callback=OC_callback, polarity=pyb.Timer.LOW)
## @brief The input compare match is set on channel 2 and is initally initialized as 'None'.
t2ch2 = tim2.channel (2, pyb.Timer.IC, pin=pinB3, callback=None, polarity=pyb.Timer.FALLING)



print('Think fast! Press the blue button on the Nucleo once the LED flashes. Press "ctrl-C" to start over and get your average reaction time. \n')
utime.sleep(1)
print('Now watch for the green light!!!')    

'''This function automatically starts the reaction-time game.
   The round begins once the LED starts blinking. The user's response time is in seconds. If the user misses their chance to press the button, then the display message will tell the user to 'try again'.
   The user may escape the game by pressing ctrl-c.
    '''

while True:
    try:
        t2ch1.callback(OC_callback)
        t2ch2.callback(IC_callback)
        t2ch1.compare(last_compare)
        deltaT = -(timeBpressed - last_compare) * (2450/80_000_000)
        
        if deltaT < 0:
            print('Missed your chance? Try again! ')
            pass
        else:
            
            deltaT = round(deltaT,3)
            if BP == True:
                _avgReactionTot  += deltaT
                run += 1
                BP = False
            
            print('Nice! Your reaction time was: ' + str (deltaT) + ' sec')
                                
    except KeyboardInterrupt:
        # Keyboard interrupt was not registering
        pass
    if run != 0:
        avg = _avgReactionTot/run
        avg = round(avg,3)
        print("--> Thanks, for playing! Your average response time was " + str(avg) + " seconds. " )
        utime.sleep(4)
    

