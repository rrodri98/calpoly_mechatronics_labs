'''
@file main.py
@brief    This will run on the Nucleo to facilitate the software handshake. The file will run on the Nucleo startup because file named main.py therefore, command execfile is not needed.
          A jumper wire connects the male header PC13 to the analog-in pin PA0. The ADC values have 12-bit resolution and they are sent back to the PC side as a batch of data once data
          collection is over.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
import pyb
from pyb import UART
import array as array
import micropython
import utime

# Allocates 200 bytes of memory for errors due to interrupt
micropython.alloc_emergency_exception_buf(200)

## @brief Will need a buffer to store the step response
response = array.array('H', (0 for index in range (500)))
## @brief Knowing the frequency at which the ADC samples gives the corresponding time stamp
t_step = 5
## @brief Will need a buffer to store the time
time = array.array('H', (0 + t_step*index for index in range (500)))

## Create serial object for communication
myuart = UART(2)

## Create an ADC on pin A0
adc = pyb.ADC(pyb.Pin.board.A0)
## Create a timer 2 object running at 200000 Hz
tim = pyb.Timer(2, freq=200000)

## @brief This parameter is initalized as False.
button_press = False

## Callback function to run when button is pressed
def button_isr (which_pin):        # Create an interrupt service routine
    '''
    @brief            This is the function that runs when the button is pressed.
    @details          This callback returns True if the button has been pressed.
    @param which_pin  This is the pin for which the interrupt service routine is set for. In this case it is Pin 13, which corresponds to the button.
    '''
    global button_press
    button_press = True
    return button_press

extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
              pyb.ExtInt.IRQ_FALLING,      # Interrupt on falling edge
              pyb.Pin.PULL_NONE,           # Activate no pullup resistor
              button_isr)                  # Interrupt service routine

'''
    @brief   This script serves as the backend of the Nucleo communication. 
    @details Data collection commences if the user sent a 'G' on the front end.
             Furthermore, data is only returned if a good response was obtained.
'''
while True:
    
    if myuart.any() != 0:
        
        # Read user input
        val = myuart.readchar()
        
        # Read from pin if G was sent
        if val == 71:
            # myuart.write('Press blue button for 3 seconds then release. \r\n')
            utime.sleep(3)

            while button_press:
                # Take ADC readings at a frequent interval
                adc.read_timed(response, tim)
                if response[0] == 0 and response[499] >= 3500:
                    for n in range(len(time)):
                        myuart.write('{:},{:}\r\n'.format (time[n], response[n]))                       
                pass
        
        # 'G' was not pressed. Prompt user to press 'G.'
        else:
            myuart.write("Press 'G' for data collection. \r\n")
            break
        