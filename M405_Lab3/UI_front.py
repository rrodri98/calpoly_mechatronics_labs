'''
@file UI_front.py
@brief The user interface front end communicates to the Nucleo through serial communication. It will prompt the user to enter 'G' for data collection.
        At this point, the Nucleo will begin waiting for a user button press. The input voltage from the button press will be stored in an array then opened
        as a batch of data on the front end. When using ADC, 0 corresponds to zero volts and 4095 represents approximately 3.3 volts.
        The data will be collected in a comma-separated variable file with the appropriate ADC count and timestamp.
        The data will be plotted using the matplotlib module in the the front end as well. 
@author Rebecca Rodriguez
@date February 20, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           See source for myformat function: https://stackoverflow.com/questions/2389846/python-decimals-format.
'''
import numpy as np
import matplotlib.pyplot as plt
import serial 
import csv

# Set up serial port. Define serial port definition, baude rate, and timeout flag
ser = serial.Serial(port='COM3',baudrate=115200,timeout=1)  

## @brief Initialize time array.
time_ = []
## @brief  Initialize ADC count array.
ADC_count = []
## @brief  Initialize voltage response array.
voltage_ = []
## @brief Set conversion factor for ADC count -> voltage.
conv_Factor = 3.3/4095

# Send an ASCII character
def sendChar():
    '''
    @brief    Responsible for requesting and reading characters from the user.
    @details  The value then gets sent to the Nucleo side to verify the appropriate 
              character was sent to initialize data collection.
    '''
    # Prompts user to enter 'G' to continue with data collection.
    inv = input('Push the right buttons! Enter G then press the blue button to begin data collection: \r\n ')
    # Character input gets converted to decimal value and returns it to Nucleo.
    ser.write(str(inv).encode('ascii'))
    
# Keep file open while writing data to it
with open('data_set2.csv','w') as csv_file:
    csv_writer = csv.writer(csv_file)
    for n in range(1):
        print(sendChar())
        for count in range (501):
            # Obtain the two serial inputs from the Nucleo side [time, ADC_count]
            val = ser.readline().decode('ascii')
            # Remove the carriage return and new line + Split data with a comma
            values = val.strip('\r\n').split(',')
            # Write to CSV file
            csv_writer.writerow(values)
            print(values)
            
            # Append values if no empty string is returned
            if val != '': 
                # Resulting values are strings
                ADC_count.append(values[1])
                time_.append(values[0])
            
            
# Convert the list of strings to list of floats
list_of_ADCfloats = [float(item) for item in ADC_count]
list_of_timefloats = [float(item) for item in time_]

# Convert the ADC count to a voltage value and round to two decimal places
for i in range(len(list_of_ADCfloats)):
    voltage_.append(round((list_of_ADCfloats[i] * conv_Factor),2))

# Close serial port
ser.close()
    
# Need to plot button response vs time
plt.plot(time_, voltage_)
plt.xlabel("Time [microsec]", fontsize = 18)
plt.xticks(np.arange(0, 500, step=50))  # Set label locations.
plt.ylabel("Voltage [V]", fontsize = 18)
plt.savefig("test.png")
plt.show()