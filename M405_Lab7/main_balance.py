'''
@file main_balance.py
@brief Closed-loop control feedback for balancing the ball on the platform.
@details 
@author Rebecca Rodriguez
@date June 11, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
from TouchScreenDriver import ScreenDriver
from motorDriver_working import MotorDriver
from bno055 import BNO055
import machine
import pyb
import utime

print('Initializing resistive touch panel...')
## A touchPanelDriver object used control the touch panel.
_scc = ScreenDriver(pyb.Pin.board.PA0, pyb.Pin.board.PA1, pyb.Pin.board.PA6, pyb.Pin.board.PA7, 18.5,10.5,1414,1500)

print('Initializing motors..')
## Motor nSLEEP pin used to enable the driver chip.
pin_nSLEEP = pyb.Pin.cpu.A15; # doesn't change

## Create a timer object used for PWM generation
tim = pyb.Timer(3, freq=20000) # doesn't change

## Create a motor object passing in the pins and timer for motor 1
moe_1 = MotorDriver(pin_nSLEEP, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, tim, 1, 2)
# Enable motor driver for motor 1
moe_1.enable()
# Set duty cycle to zero for motor 1
moe_1.set_duty(0)
    
## Create a motor object passing in the pins and timer for motor 2
moe_2 = MotorDriver(pin_nSLEEP, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, tim, 3, 4)
# Enable motor driver for motor 2
moe_2.enable()
# Set duty cycle to zero for motor 2
moe_2.set_duty(0)
 
print('Initializing IMU..')
## Specify I2C channel used
i2c = machine.I2C(1)
## Create an IMU object 
imu = BNO055(i2c)

print('Initializing the closed-loop controller..')
## Gain matrix in the form of [K_1 K_2 K_3 K_4] for motor 1
gains1 = [-3, 0,-9, 0]
## Gain matrix in the form of [K_1 K_2 K_3 K_4] for motor 2
gains2 = [-3, 0,-9, 0]

## Resistance of motor [ohms]
R = 2.21 
## Motor torque constant [mNm/A]
Kt = 13.8
## Voltage supplied to the motor [V]
Vdc = 12 
## Reduction gear ratio
N = 4
## Conversion factor for required torque of motor to percent duty of motor
percent_duty_convFactor = (100 * R) / (N * Kt * Vdc)

## Resistive touch panel measured ball position (x, y) [m]
x, y = 0, 0

## Resistive touch panel measured ball velocity (x dot, y dot) [m/s]
x_dot, y_dot = 0, 0

## Platform angle (theta x, theta y) [rad]
theta_x, theta_y = 0, 0

## Platform angular velocity (theta dotx, theta doty) [rad/s]
theta_dotx, theta_doty = 0, 0

if __name__ == '__main__':   
    print('Please balance the ball at the center of the platform...')
    
    while True:
        # try:
        #     if xyz[2] == False:
        #         print('Please balance the ball at the center of the platform...')
        #     elif xyz[2] == True:
        #         print('Thanks! Let the balancing begin!')
        # except:
        #     pass
        
        ## Initial state of state diagram.
        state = 0
        
        if (state == 0):
        ## Interval between transitions.
            interval = 100
            ## Initial time obtained in milliseconds.
            t_initial = utime.ticks_ms()
            ## Final time calculated in milliseconds.
            t_final = utime.ticks_add(t_initial, interval)
            _xyz = _scc.quickScan()
            _euler = imu.euler()
            ## Initial x location of the ball.
            x = _xyz[0]
            ## Initial y location of the ball.
            y = _xyz[1]
            ## Initial platfrom angle about the x-axis.
            theta_x = _euler[1]
            ## Initial platfrom angle about the y-axis.
            theta_y = _euler[2]
            
            _Done = False
            
            while _Done == False:
                
                if utime.ticks_ms() >= t_final:
                    _xyz = _scc.quickScan()
                    _euler = imu.euler()
                    ## Final x location of the ball.
                    xF = _xyz[0]
                    ## Final y location of the ball.
                    yF = _xyz[1]
                    ## Final platfrom angle about the x-axis.
                    theta_xF = _euler[1]
                    ## Final platfrom angle about the y-axis.
                    theta_yF = _euler[2]
                    
                    # Calculates x_dot in m/s for a ms interval scanX wait time [note mm/ms = m/s]
                    x_dot = (xF - x)/ (interval * 10)
                    y_dot = (yF - y)/ (interval * 10)
                    theta_dotx = (theta_xF - theta_x)/ (interval * 10)
                    theta_doty = (theta_yF - theta_y)/ (interval * 10)
                    print(x_dot, y_dot,theta_dotx, theta_doty)
                    _Done = True
                    
                else:
                    pass
                state = 1
        if (state == 1):
        ## Calculated torque required for motor 1
            Tm_1 = -(gains1[0]*yF + gains1[1]*y_dot + 
                    gains1[2]*theta_xF + gains1[3]*theta_dotx)
        ## Required percent duty for motor 1
            L_1 = percent_duty_convFactor * Tm_1
    
            moe_1.set_duty(L_1)
            ## Calculated torque required for motor 2
            Tm_2 = -(gains2[0]*xF + gains2[1]*x_dot + 
                        gains2[2]*theta_yF + gains2[3]*theta_doty)
            ## Required percent duty for motor 2
            L_2 = percent_duty_convFactor * Tm_2
    
            moe_2.set_duty(L_2)
            state = 0