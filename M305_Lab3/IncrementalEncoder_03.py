'''
@file IncrementalEncoder_03.py
@brief This is the user task used in conjucntion with the @ref shares.py file to facilitate data communication over the serial port.

@details The encoder has two channels. These two channels are polled and may be used to detail the 
          encoder position. The resulting information may be used to determine the amount or direction of the
          rotation of the motor. The user communication is through the REPL window using putty. 
          In the next lab, the user interface will be extended by allowing communication between
          the PC side and the Nucleo.
          
@author Rebecca Rodriguez
@date October 13, 2020 updated July 14, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
from pyb import UART
import utime
        
class UserTask:
    '''
    @brief   User interface task.
    @details  Implement a FSM to accept user input and return encoder results.
    '''
    ## Initialization state.
    S0_INIT             = 0
    
    ## Waiting for character state.
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state.
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, taskNum, interval, dbg=False):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task.
        @param interval An integer number of microseconds between desired runs of the task.
        @param dbg A boolean indicating whether the task should print a trace or not.
        '''
        ## The number of the task.
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task.
        self.interval = int(interval*1e6)
        
        ## Flag to print debug messages or supress them.
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run.
        self.runs = 0
        
        ## The timestamp for the first iteration.
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next.
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Create serial object for communication.
        self.myuart = UART(2)
        
        self.myuart.write('This is the encoder lab! Press z to zero the encoder position. Press p to print the encoder position. Press d to print the encoder delta.\r\n')
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task.
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.printTrace()
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                # Run State 1 Code
                self.printTrace()
                if self.myuart.any():
                    shares.cmd = self.myuart.readchar()
                    # self.myuart.write('Received a character!!! \r\n')
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                self.printTrace()
                if shares.resp:
                    # self.myuart.write('Received a response!!! \r\n')
                    self.myuart.write(shares.resp)
                    shares.resp = None
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            ## Specifying the next time the task will run.
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)