'''
@file main_Lab7.py
@brief    This script serves as the backend of the Nucleo communication. 
@details    This file imports the encoder driver, motor driver, and controller. A finite state machine is used to continually update the proportional constant from the command sent by the user.
@author Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date March 7, 2021
'''
import pyb
from pyb import UART
import micropython
import utime
from encoderDriver_by_RR import EncoderDriver
from motorDriver_working import MotorDriver
from controller import ClosedLoop, ClosedLoopTask
import shares_Lab6


micropython.alloc_emergency_exception_buf(200)
## Create pin objects used for interfacing with the encoder driver for channel 1.
pin_EIN1 = pyb.Pin.cpu.B6;   
## Create pin objects used for interfacing with the encoder driver for channel 2.
pin_EIN2 = pyb.Pin.cpu.B7;    
# Create pin object used for the sleep pin of the motor driver.
pin_nSLEEP = pyb.Pin.cpu.A15; 
# Create pin object used for the fault pin of the motor driver.
pin_nFAULT = pyb.Pin.cpu.B2;  
## Create pin object used for interfacing with the motor driver for channel 1.
pin_MIN1 = pyb.Pin.cpu.B4;    
## Create pin object used for interfacing with the motor driver for channel 2.
pin_MIN2 = pyb.Pin.cpu.B5;     
## Create a timer object for polling the encoder channels.
tim_1_2 = 4;                      
## Create a timer object used for PWM generation.
tim = pyb.Timer(3, freq=20000) 
## Create an encoder object passing in the pins and timer for motor 1.
enc1 = EncoderDriver(pin_EIN1, pin_EIN2, tim_1_2)
## Create a motor object passing in the pins and timer for motor 1. Where 1 and 2 are the channels used for the timer selected.
moe_1 = MotorDriver(pin_nSLEEP, pin_MIN1, pin_MIN2, tim, 1, 2)
moe_1.enable()
moe_1.set_duty(0)
## Define starting proportional gain for controller.
shares_Lab6.Kp = 0.1
## Define starting pulse width modulation level.
shares_Lab6.L = 0
## Create a closed loop object passing in the proportional constant.
CL = ClosedLoop(shares_Lab6.Kp)
## Create a closed loop task object passing in the closed loop, timer, and motor object for motor 1.
CLtask2 = ClosedLoopTask(CL,enc1,moe_1,0.02)
## Measured RPM of the encoder for motor 1.
omega_meas = 0
## Reference angular velocity set for tuning Kp. Value is in rev/min.
shares_Lab6.omega_ref = 150 #RPM
## Time in milliseconds between measuring encoder data.
interval = 20
start_time = utime.ticks_ms()
## Next time to run the task.
next_time = utime.ticks_add(start_time, interval)
## Create serial object for communication.
myuart = UART(2)
## Initial FSM state.
S0_INIT = 0
## Second FSM state for waiting for user response.
S1_WAIT = 1
## Third FSM state for controlling the board using the user supplied proportional constant value.
S2_CTL = 2
state = S0_INIT
## Initialize the number of runs.
run = 0
## Record the initial starting time in milliseconds.
start_time = utime.ticks_ms()


while True:
    ## Obtain the current time in milliseconds.
    currTime = utime.ticks_ms()
        
    if utime.ticks_diff(currTime, next_time) >= 0:
        # If the interval has been reached
        if state == S0_INIT:
            # Run state 0 code
            ## Initialize encoder position to zero.
            enc1.set_position()
            state = S1_WAIT
            
        elif state == S1_WAIT:
            # Run state 1 code - set Kp
            CL.set_Kp(shares_Lab6.Kp)
            enc1.update()
            shares_Lab6.omega_meas = enc1.get_delta() * 10**3 * 60 / (interval * 4000) # Calculate rpm
            # if run > 40:
            #     shares_Lab6.omega_ref = 500
            # else:
            #     shares_Lab6.omega_ref = 0
            state = S2_CTL
            
        elif state == S2_CTL:
            # Run state 2 code
            CL.run(shares_Lab6.omega_ref,shares_Lab6.omega_meas)
            moe_1.set_duty(shares_Lab6.L*3.3)     # Set PWM level for motor 1
            myuart.write('{:2.2f},{:2.2f},{:2.2f}\r\n'.format(shares_Lab6.omega_meas,shares_Lab6.omega_ref,(utime.ticks_ms()-start_time)*1e-3))
            state = S1_WAIT
        ## Specifying the next time the task will run
        next_time = utime.ticks_add(next_time, interval)
        run += 1

  



    
 