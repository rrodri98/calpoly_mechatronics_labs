'''
@file UI_front_Lab7.py
@brief The user interface front end communicates to the Nucleo through serial communication. It will prompt the user to enter a proportional constant 'Kp' for data collection.
@details  At this point, the Nucleo will begin waiting for a user button press. The angular velocity and timestamps will be stored in an array then opened
        as a batch of data on the front end. 
        The data will be plotted using the matplotlib module in the the front end as well. 
@author Rebecca Rodriguez
@date February 20, 2021
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
           See source for myformat function: https://stackoverflow.com/questions/2389846/python-decimals-format.
'''
import numpy as np
import matplotlib.pyplot as plt
import serial 
import time
import csv
import shares_Lab7

## Set up serial port. Define serial port definition, baude rate, and timeout flag.
ser = serial.Serial(port='COM4',baudrate=115200,timeout=1)  

## @brief Empty array to store time values.
time_ = []
## @brief  Empty array to store reference position values.
posRef = []
## @brief  Empty array to store reference angular velocity values.
omegaRef = []
## @brief  Empty array to store reference position values.
pos = []
## @brief  Empty array to store reference angular velocity values.
omega = []
## @brief Empty array to store time values.
_time = []

with open('reference.csv','r') as csv_file:
    ## CSV header names.
    fieldnames = ['time,' 'ang. vel', 'angle']
    ## Object for csv file.
    csv_reader = csv.DictReader(csv_file)
    for line in csv_reader:
        omegaRef.append(line['ang. vel'])
        posRef.append(line['angle'])
        time_.append(line['time'])
        
        
## Convert the list of position strings to list of floats for plotting.
list_of_posREf_floats = [float(item) for item in posRef]
## Convert the list of reference omega strings to list of floats for plotting.
list_of_omegaREf_floats = [float(item) for item in omegaRef]
## Convert the list of time strings to list of floats for plotting.
list_of_timefloats = [float(item) for item in time_]

# Send an ASCII character
def sendChar():
    '''
    @brief    Responsible for sending and reading characters from the user.
    @details  The reference position or angular velocity value gets sent to the Nucleo side.
    '''
    # Prompts user to enter a proportional constant 'Kp' to continue with data collection.
    inv = input("Please enter a Kp value to begin': \r\n ")
    # Character input gets converted to decimal value and returns it to Nucleo.
    for position,omega in zip(list_of_posREf_floats,list_of_omegaREf_floats):
        ## Variable used to store the serial inputs. The input from the Nucleo side is in the form [time, angular velocity].
        ser.write(str(position).encode('ascii'))
        shares_Lab7.pos_ref = position
        shares_Lab7.omega_ref = omega
    
for n in range(1):
    # print(sendChar())
    sendChar()
    for count in range(list_of_timefloats):
        ## Variable used to store the serial inputs. 
        val = ser.readline().decode('ascii')
        ## Remove the carriage return and new line and split data with a comma.
        values = val.strip('\r\n').split(',')
        # Append values if no empty string is returned
        time.sleep(0.5)
        if val != '': 
            # Resulting values are strings
            omega.append(values[2])
            pos.append(values[1])
            _time.append(values[0])
            
## Convert the list of position strings to list of floats for plotting.
list_of_pos_floats = [float(item) for item in posRef]
## Convert the list of reference omega strings to list of floats for plotting.
list_of_omega_floats = [float(item) for item in omegaRef]
## Convert the list of time strings to list of floats for plotting.
list_of_time_floats = [float(item) for item in time_]
            
            
_fig, _axs = plt.subplots(2)
_fig.suptitle('Reference Profiles')
_axs[0].plot(list_of_timefloats, list_of_posREf_floats, list_of_pos_floats)
_axs[1].plot(list_of_timefloats, list_of_omegaREf_floats, list_of_omega_floats)

# CALCULATE 'J' - THE PERFORMANCE METRIC
_deltaP = []
_deltaO = []
## Variable for the performance metric. Should be close to zero.
J = 0
for p,pR in zip(list_of_pos_floats, list_of_posREf_floats):
    _delta_pos = (pR - p)**2
    _deltaP.append(_delta_pos)
    
for o,oR in zip(list_of_omega_floats, list_of_omegaREf_floats):
    _delta_omega = (oR - o)**2
    _deltaO.append(_delta_omega)

for dO,dP in zip(_deltaO, _deltaP):
    J += (dO + dP)
    
print('The performance metric is: '+str(J))
